﻿Public Class Song

    Public Property Song As String
    Public Property Artist As String
    Public Property Album As String
    Public Property FileName As String
    Public Property FilePath As String

    Public Sub New(fileName As String, artist As String, album As String, song As String)
        Me.FileName = fileName
        Me.Artist = artist
        Me.Album = album
        Me.Song = song
    End Sub

    Public Function SongAsRow(ByRef dataTable As DataTable) As DataRow
        Dim songRow As DataRow = dataTable.NewRow()
        songRow.Item(0) = Me.FileName
        songRow.Item(1) = Me.Artist
        songRow.Item(2) = Me.Album
        songRow.Item(3) = Me.Song
        SongAsRow = songRow
    End Function

End Class
