﻿Public Class M4AReader

    Private tableColumns() As String = {"File Name", "Artist", "Album", "Song"}
    Private dataTable As DataTable
    Private songsList As New List(Of Song)
    Delegate Sub SetTextCallBack([text] As String)

    Private Sub M4AReader_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Me.LoadTable()
        Me.btnRename.Enabled = False
    End Sub

    Private Sub DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        Me.LoadSongs()
    End Sub


    Private Sub EventHandler(sender As Object, e As EventArgs) Handles btnSelectFolder.Click, menuExit.Click
        Select Case sender.Name
            Case btnSelectFolder.Name
                Me.PopulateRowsFromSelection()
            Case menuExit.Name
                Me.Close()
        End Select
    End Sub

    Private Sub LoadTable()
        Me.dataTable = New DataTable()

        ' Populate Table
        For i = 0 To Me.tableColumns.Length - 1
            Dim column As DataColumn = New DataColumn(Me.tableColumns(i))
            Me.dataTable.Columns.Add(column)
        Next

        ' Add blank rows
        For i = 0 To 9
            Dim row As DataRow = Me.dataTable.NewRow()
            Me.dataTable.Rows.Add(row)
        Next

        ' Me.dataGrid.DataSource = Me.dataTable

    End Sub

    Private Sub PopulateRowsFromSelection()
        Dim dialogResult As DialogResult = Me.folderBrowser.ShowDialog()

        If (dialogResult = Windows.Forms.DialogResult.OK) Then
            Dim folderPath As String = Me.folderBrowser.SelectedPath
            Me.txtFolderPath.Text = folderPath
            Try
                ' Me.LoadSongs()
                ' Trigger backgroundworker
                bw.RunWorkerAsync()
            Catch ex As Exception
                MessageBox.Show(ex.Message & "Exiting Now")
                Me.Close()
            End Try

        End If
    End Sub

    Private Sub LoadSongs()
        Dim breaker As String = "m4a"
        Dim table As DataTable = New DataTable()
        Dim artist As String = " "
        Dim album As String = " "
        Dim name As String = " "
        ' Me.dataTable.Clear()
        Dim rootFolder As New IO.DirectoryInfo(Me.txtFolderPath.Text)
        Dim listOfFiles As IO.FileInfo() = rootFolder.GetFiles()
        Dim listOfFolders() As IO.DirectoryInfo = rootFolder.GetDirectories()
        Dim i As Integer = 0
        For Each folder In listOfFolders
            listOfFiles = folder.GetFiles()
            For Each file In listOfFiles
                Try
                    If (file.Name.Chars(0) = ".") Or (Not (file.Name.Contains(breaker))) Or (file.Name.IndexOf("-") <> -1) Then
                        Continue For
                    End If
                    QTController.URL = file.FullName
                    Dim qtFile As QTOLibrary.QTMovie = QTController.Movie
                    artist = qtFile.Annotation(1634890867)
                    album = qtFile.Annotation(1634493037)
                    name = qtFile.Annotation(1851878757)
                    ' My.Computer.FileSystem.RenameFile(file.FullName, artist & " - " & name & ".m4a")
                    Me.SetText(artist & " - " & name & ".m4a - FOUND" & vbNewLine)
                    ' txtLog.AppendText(artist & " - " & name & ".m4a - FOUND" & vbNewLine)
                    songsList.Add(New Song(file.FullName, artist, album, name))
                Catch ex As Exception
                    Me.SetText(artist & " - " & name & ".m4a - FAILED to update" & vbNewLine)
                    ' txtLog.AppendText(artist & " - " & name & ".m4a - FAILED to update" & vbNewLine)
                    Continue For
                End Try
                ' Dim song As New Song(qtObject.Get)
            Next
        Next
        QTController.Dispose()
        For Each currSong In songsList
            Try
                My.Computer.FileSystem.RenameFile(currSong.FileName, currSong.Artist & " - " & currSong.Song & ".m4a")
                Me.SetText(currSong.Artist & " - " & currSong.Song & ".m4a - Successfully UPDATED" & vbNewLine)
                '  txtLog.AppendText(currSong.Artist & " - " & currSong.Song & ".m4a - Successfully UPDATED" & vbNewLine)
            Catch ex As Exception
                Me.SetText(currSong.Artist & " - " & currSong.Song & ".m4a - update FAILED" & vbNewLine)
                Me.SetText(ex.Message & vbNewLine)
                ' txtLog.AppendText(currSong.Artist & " - " & currSong.Song & ".m4a - update FAILED" & vbNewLine)
                ' txtLog.AppendText(ex.Message & vbNewLine)
            End Try

        Next

        ' Me.btnRename.Enabled = True
    End Sub

    Private Sub SetText([text] As String)
        If Me.txtLog.InvokeRequired Then
            Dim d As New SetTextCallBack(AddressOf SetText)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtLog.AppendText([text])
        End If
    End Sub

End Class
